import { Component } from '@angular/core';
import {Paho} from 'ng2-mqtt/mqttws31';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
//CLIENTE WEB HIVE MQTT
//http://www.hivemq.com/demos/websocket-client/
//libreria mqtt javascript
//https://www.sakib.ninja/angular2-with-mqtt/
export class HomePage {
  client;
  constructor() {
    this.client = new Paho.MQTT.Client('broker.mqttdashboard.com', 8000, 'clientId-TZPSQgBaAo');

    this.onMessage();
    this.onConnectionLost();
    this.client.connect({onSuccess: this.onConnected.bind(this)});
  }
  onConnected() {
    console.log("Connected");
    this.client.subscribe("testtopic/1");
    this.sendMessage('HelloWorld');
  }

  sendMessage(message: string) {
    let packet = new Paho.MQTT.Message(message);
    packet.destinationName = "123456";
    this.client.send(packet);
  }

  onMessage() {
    this.client.onMessageArrived = (message: Paho.MQTT.Message) => {
      console.log('Message arrived : ' + message.payloadString);
    };
  }

  onConnectionLost() {
    this.client.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost : ' + JSON.stringify(responseObject));
    };
  }

}
